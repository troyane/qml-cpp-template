#include <QtGlobal>
#include <QtGui/QGuiApplication>
#include <QtQml/QQmlApplicationEngine>
#include <QDebug>
#include <QtQml>

#include "cppclass.h"

int main(int argc, char *argv[]) {
    QGuiApplication app(argc, argv);
    // 1 case:
    // Register type and create object at QML side
    qmlRegisterType<CppClass>("CppClassModule", 1, 0, "CppClass");
    QQmlApplicationEngine engine(QUrl("qrc:///qml/main.qml"));
    qDebug() << "Qt version: " << qVersion();
    // 2 case:
    // Create object here and "move it up" to QML side
    // engine.rootContext()->setContextProperty("cppClass", new CppClass);
    return app.exec();
}
